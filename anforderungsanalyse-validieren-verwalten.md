## Anforderungsanalyse (_requirement engineering_)


<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/anforderungsanalyse-validieren-verwalten</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109422853120283884</span>

> **tl/dr;** _(ca. 11 min Lesezeit): Nicht alle Anforderungen sind gleich wichtig, nicht alle Anforderungen sind auf Anhieb gut formuliert/dokumentiert. Woran erkenne ich gute Anforderungen, woran erkenne ich wichtige Anforderungen? Und: gibt es auch zu viel Spezifikation?_

Dieser Artikel ist Bestandteil einer mehrteiligen Reihe zu _Requirements Engineering_:

- Grundlagen Anforderungsanalyse: [Wozu dient Anforderungsanalyse und welche Anforderungsarten und Qualitätsmerkmale gibt es?](https://oer-informatik.de/anforderungsanalyse)

- Haupttätigkeit Ermittlung: [Woher kommen Anforderungen und mit welchen Techniken ermittle ich sie systematisch?](https://oer-informatik.de/anforderungsanalyse-ermittlung)

- Haupttätigkeit Dokumentation: [Auf welche Arten können Anforderungen dokumentiert werden?](https://oer-informatik.de/anforderungsanalyse-dokumentieren)

- (**dieser Artikel**) Haupttätigkeiten Validierung und Verwaltung: [Wie kann die Qualität der Anforderungen überprüft werden, wie priorisiere ich sie, und wie  verwalte ich sie im Projektverlauf?](https://oer-informatik.de/anforderungsanalyse-validieren-verwalten)

- [Übungsaufgaben zur Anforderungsanalyse](https://oer-informatik.de/anforderungsanalyse-uebungsaufgaben)

### Haupttätigkeit 3: Prüfen und Bewerten der Anforderungen (_requirements validation_)

Bei der Prüfung und Validierung der Anforderungen stehen besonders zwei Fragen im Raum:

- Ist ein gemeinsames Verständnis über die Anforderung hergestellt worden?

- Ist die Anforderung wertvoll - auch mit sich ggf. änderndern Randbedingungen?

- Gibt es Konflikte zwischen Anforderungen (unterschiedlicher Stakeholder)?

Der Validierungsprozess ist von fundamentaler Wichtigkeit im Requirement-Engineering und wird im 6. Prinzip des IREB zusammengefasst als: Validierung - Nicht validierte Anforderungen sind nutzlos.

#### Gemeinsames Verständnis

Bereits vor der Durchführung sollte sichergestellt werden, dass die dokumentierten Anforderungen von allen _Stakeholder_ gleich verstanden werden und deren Bedürfnisse erfüllen.

Das gemeinsame Verständnis kann über eine gemeinsame Sprache sichergestellt werden. Hierbei geht es v.a. um die Bedeutung projektspezifischer Begriffe. Diese Sprache sollte konsequent in bestehenden Handbüchern / Dokumentationen des Projekt genutzt werden. Bei größeren Projekten kann es daher hilfreich sein, in einem eigenen, für alle Beteiligten einsehbaren Glossar die Bedeutung von Begriffen festzuschreiben.

Neben diesem expliziten gemeinsamen Verständnis gibt es v.a. bei kleineren Projekten und in kleinen vertrauten Teams ein implizites gemeinsames Verständnis. Erst dieses Verständnis ermöglicht viele agile Praktiken, schafft und benötigt Vertrauen.

Je größer ein Team ist, je seltener gemeinsame Treffen sind (Homeoffice/ geographische Entfernung), auf je mehr Teams/Firmen sich das Projekt aufteilt, desto schwerer lässt sich ein implizites gemeinsames Verständis herstellen. Das Verhältnis aus explizitem zu implizitem Wissen kann sich also - auch im Projektverlauf - ändern.

Das Risiko des Scheiterns von Projektbestandteilen kann so minimiert werden.

Das gemeinsame Verständnis ist das 3. Prinzip des Requirement Engineerings nach dem IREM: Erfolgreiche Systementwicklung ist ohne eine gemeinsame Basis nicht möglich.

#### Wertvolle Anforderungen

Nach der Anforderungsermittlung gibt es möglicherweise eine Unmenge an detaillierten Anforderungen. Nicht alle sind zum jeweiligen Projektzeitpunkt wertvoll und produzieren einen Nutzen.

Auf der anderen Seite erzeugt das Ermitteln, Dokumentieren, Validieren und Verwalten von Anforderungen Aufwände. Es sollte daher eine Kosten/Nutzen Abschätzung für die Anforderungen durchgeführt werden.

Das erste Prinzip des Requirement Engineering nach dem IREB ist die Wertorientierung: Anforderungen sind Mittel zum Zweck, kein Selbstzweck.

#### Qualitätskriterien für Anforderungen

Im Rahmen der Validierung sollte auch überprüft werden, ob Anforderungen gut definiert wurden. Hierzu können beispielsweise die Qualitätskriterien für Anforderungen genutzt werden, wie sie im IREB-Lehrplan^[[Download des Lehrplans hier](https://www.ireb.org/content/downloads/2-cpre-foundation-level-syllabus-3-0/cpre_foundationlevel_syllabus_de_v.3.1.pdf)] genannt werden. Anforderungen sollten demnach

- adäquat (beschreibt echte und abgestimmte Bedürfnisse der Stakeholder),

- notwendig,

- eindeutig,

- vollständig (in sich abgeschlossen),

- verständlich und

- prüfbar sein.


Im Zusammenspiel mit anderen Anforderungen sollten alle Anforderungen

- konsistent,

- nicht redundant,

- vollständig (keine bekannten und relevanten Anforderungen versäumt),

- modifizierbar,

- verfolgbar und

- konform sein.

Viele dieser Kriterien haben wir an anderer Stelle bereits kennengelernt (z.B. im Rahmen der I.N.V.E.S.T.-Kriterien).

Im Rahmen der Validierung sollten die folgenden Fragen beantwortet werden:

* Wurden Konflikte zwischen konkurrierenden Anforderungen / _Stakeholdern_ identifiziert und aufgelöst?

* Entspricht die Priorisierung der Anforderungen dem Wunsch des Kunden oder ist mit diesem einvernehmlich abgesprochen?

* Wurden die Wünsche und Bedürfnisse der _Stakeholder_ durch die dokumentierten Anforderungen hinreichend abgedeckt?

* Wurde ein gemeinsames Verständnis der Anforderung bei allen Projektbeteiligten erreicht?

* Gibt es Einvernehmen über die Systemgrenzen und über die Randbedingungen?

* Haben sich Anforderungen geändert? (z.B. Gesetzestexte, Konkurrenzprodukte, neue Technologien, Prioritätsänderungen, Feedback von Stakeholdern)

Der IEEE-Standard für _Software Requirements Specification_ (IEEE 830) ergänzt die Kriterien an die Spezifikation zusätzlich um das Kriterium "bewertet nach Wichtigkeit und/oder Stabilität".

#### Review- Techniken zur Validierung von Anforderungen

* Walkthrough

* Stellungnahme

* Inspektion: Systematische Qualitätssicherung

Tracability

#### Gliedern und Priorisieren von Anforderungen

Die Gliederung von Anforderungen kann aus unterschiedlichen Gründen sinnvoll sein:

* Im Vorfeld, um ggf. fehlende Anforderungen in Kategorien zu erkennen.

* In der Bearbeitung, um Ansatzpunkte für eine Priorisierung zu erhalten oder eine Reihenfolge festzulegen.

* In der Abstimmung unterschiedlicher Stakeholder, um Konflikte aufzulösen.


Es gibt eine Vielzahl an möglichen Herangehensweisen

Bewertung / Priorisierung

- Geschäftswert

- Dringlichkeit

- Aufwand

- Abhängigkeiten

- Größe

Gliederung nach Systemkomponenten:

* Aufteilung nach Abstraktionsebenen (geringer Abstraktionsgrad: "Nah am User" / hoher Abstraktionsgrad: "Nah an den Bits und Bytes")

* Aufteilung nach dem Schichtenmodell: Persistenzschicht (Datenhaltung), Verabeitungsschicht (Logik) und Präsentationsschicht (UserInterface)

##### Gliederung nach Realisierungspriorität:

Aufteilung in optionale oder zwingende Anforderungen (wird oft im Pflichtenheft genutzt)

* Erweiterung durch **MoSCoW-Methode**:

|Must have|Should have| Can have|Won’t have|
|--|--|--|--|
|"muss"|"soll"|"wird"|"kann"|
|Erste Priorität: nicht verhandelbar, bindend, ein Fehlen würde zu Projetscheitern führen. MUST = Minimal usable Subset|Zweite Priorität: hohe Relevanz, dringend empfohlen, sollten wenn immer möglich realisiert werden. Nichterfüllung hätte folgen und muss begründet werden.|"Nice to have": Werden umgesetzt, wenn noch Kapazitäten (Zeit/Ressourcen) vorhanden sind. Wichtig, um über Standardleistungen hinauszugehen.|Zeitlich nicht kritische Anforderungen, die aber technisch sinnvoll / wichtig sind. Bilden die Grundlage für nächste Produktiterationen|

* Mit dem **KANO-Modell** werden Anforderungen per Umfrage gewertet und in Kategorien eingeteilt:

|Basis-Merkmale|Leistung-Merkmale|Begeisterungs-Merkmale|Unerhebliche Merkmale|Rückweisungs-Merkmale|
|--|--|--|--|--|
|so grundlegend, dass nur ihr Fehlen bemerkt wird, nicht jedoch, dass sie umgesetzt wurden.|Der Grad ihrer Umsetzung schafft Zufriedenheit, ihre Umsetzung wird erwartet|Überraschende Funktionen, die nicht erwartet wurden und die von der Konkurrenz abheben|Umsetzung führt nicht zu Zufriedenheit, fehlen auch nicht zu Unzufriedenheit|Realisierung führt zu Unzufriedenheit - |

##### Kategorisierung / Gliederung nach der Größe der Anforderung

T-Shirt-Size Methode: Ähnlich dem Planning-Poker wird hier jeder Anforderung eine Abstrakte, bewusst ungenau gehaltene Größe hinterlegt. Im Idealfall spielt sich ein Team nach einiger Zeit selbst auf eine "Währung" ein, andernfalls werden Zeitangaben als Referenz festgelegt, oft unkonkret, z.B.:

|XS|S|M|L|XL|XXL|
|---|---|---|---|---|---|
|ein Dev, eine Handvoll Stunden|ein Dev, früh begonnen, abends eingecheckt|x Devs, mehrere pro Sprint möglich|alle Devs, in einem Sprint für das Team möglich|muss über mehrere Sprints realisiert werden|zu groß zum schätzen|

##### Kategorisierung nach Projektrisiko:

Anforderungen mit dem höchsten Projektrisiko (deren Scheitern den Projekterfolg verhindern kann) sollten möglichst früh umgesetzt werden. Gegebenenfalls wird mit einem Wegwerf-Prototyp zunächst überprüft, ob die gewünschten Ziele erreichbar sind.

##### Kategorisierung nach evolutionärer Realisierung

Es werden diejenigen Anforderungen zusammengetragen, die kleinste Einheit eines Produkts darstellen, das bereits einen Nutzen für den Anwender erzeugt. Dieses **Minimal Viable Product** (M.V.P.) ist oft der Ausgangspunkt von iterativ-inkrementellem Vorgehen.


* Aufteilung als **Prototyp**
  * vertikaler Prototyp: enthält bereits in der ersten Iteration einen "Durchstich" durch alle Softwareschichten
  * horizontaler Prototyp: Realisierung einer Schicht / einer Teilkomponente, die nicht durch alle Schichten führt

##### Kategorisierung nach abrechenbaren Meilensteinen

Aufteilung in Milestones (Gruppierung z.B. um Rechnungsstellung innerhalb der Bearbeitung zu ermöglichen)


##### Kategorisierung nach Detaillierungsgrad als Ziel, Szenario oder lösungsorientierte Anforderung

Diese Einteilung lenkt den Blick darauf, wie Granular der Detailiierungsgrad der Anforderungen ist.^[Diese Einteilung wurde übernommen vom Buch "Basiswissen Requirement Engineering" und Fortbildungsunterlagen entnommen]

**Ziele** lassen sich in sehr frühen Phasen formulieren und validieren. Sie spiegeln ein vom Stakeholder gewünschtes Merkmal des Systems wieder und können als solches bereits dabei helfen, Konflikte früh aufzudecken und aufzulösen. Sie beantworten dei Frage: "Was soll das System bieten?"

**Szenarien** formulieren gewünschtes oder nicht gewünschtes Verhalten durch eine Abfolge von Ereignissen. Sie spiegeln oft die Sicht eines Stakeholders wieder und helfen daher beim Verständnis. Kompakte Formulierungen von Szenarien sind beispielsweise User Stories, bei der präzisierung von Use Cases werden sie detaillierter tabellarisch oder in Stichpunkten erfasst. Sie beantworten die Frage: "Wie soll das System genutzt werden?"

**Lösungsorientierte Anforderungen** sind bereits sehr technisch beschrieben, und geben nicht nur das Ziel (was?) sondern auch eine Lösungsstrategie wieder. Sie beantworten die Frage "Wie sollen Anforderungen umgesetzt werden?"

### Haupttätigkeit 4: Verwalten der Anforderungen (_requirements management_)

Im Projektverlauf werden sich die Anforderungen an das System ändern (neue Erkenntnisse, besseres Systemverständnis). Bei länger laufenden Projekten ändern sich auch die Randbedingungen (Gesetzesänderungen, Innovationen, Konkurrenzprodukte,...). Ein gutes Requirement Management hat dies vorab und im Verlauf im Blick:

* Haben sich die Randbedingungen geändert bzw. wie werden sie sich realistischerweise ändern?

* Haben sich die Anforderungen geändert (oder ist dies zu erwarten)?

* Gibt es ein neues Verständnis des Systemkontexts? Sind Teile des Umfelds irrelevant geworden, müssen andere beachtet werden?

* Ist ein gemeinsames Verständnis weiterhin gegeben?

* Besteht der Nutzen der Anforderungen weiterhin, oder ergibt sich aus anderen Anforderungen bislang unberücksichtigter Nutzen?

* Haben sich die Bedürfnisse der Stakeholder geändert?

* Muss die Priorisierung angepasst werden?

* Hat sich das Kosten/Nutzen Verhältnis der Anforderungen geändert?

* Gibt es technologische Neuerungen, die berücksichtigt werden muss?

* Gab es neues Feedback (z.B. von Stakeholdern), dass eingearbeitet werden muss?

* Wurden Fehler entdeckt, auf die reagiert werden muss?

##### Konfliktmanagement

Aus den sich (potenziell) ändernden Anforderungen und Randbedingungen können sich eine ganze Reihe von Konflikten und Problemen ergeben, die durch ein gutes Anforderungsmanagement leichter gelöst werden können.

Konflikte werden in vier Phasen bearbeitet: Identifikation, Analyse, Lösung und Dokumentation.

Abhängig davon, in welchen Bereichen die Konflikte auftreten unterscheidet man Sachkonflikte, Interessenkonflikte, Beziehungskonflikte, Datenkonflikte, Wertekonflikte und strukturelle Konflikte.

Für den Requirement Engineer bieten sich eine Reihe von Konfliktlösungstechniken:

* Einigung

* Kompromiss

* Abstimmung

* Variantenbildung

* Ober-sticht-Unter (Eskalation)

##### Veränderung als Prinzip

Das IREB hat diesem Punkt das **7. Prinzip** des Anforderungsmanagements gewidmet: **Evolution - sich ändernde Anforderungen sind kein Unfall, sondern der Normalfall.**

Eine gute Dokumentation, eine regelmäßige und gute Kommunikation im Team und mit dem Kunden sind hierbei von Vorteil.

Anforderungen müssen in unterschiedlichen Projektsituationen für unterschiedliche Rollen strukturiert, aufbereitet und konsistent geändert werden.

* Zulassen, dass Anforderungen sich ändern

* Anforderungen stabil halten

Grundsätzlich werden alle auftretenden Änderungen als Ausgangspunkt für einen Prozess gesehen, der dem Prinzip **Problem - Anforderung - Lösung** folgt. Dieser Dreischritt ist so wichtig, dass das IREB dieses Tripel das **5. Prinzip** des Requirement Engineerings genannt hat.

Neben dem Managen der eigentlichen Anforderungen muss daher auch ein Chance Management im Projekt installiert werden - in größeren Projekten durch ein Change Control Board (CCB) oder Change Advisory Board (CAB).

Gutes Requirement Engineering versucht auch immer, die Wünsche und Bedürfnisse besser zu erfüllen, als diese es erwarten (siehe u.a. Begeisterungsmerkmale im KANO-Modell). Das **Prinzip 9** des Requirement Engineerings heißt daher: **Innovation - Mehr vom Gleichen ist nicht genug**.


### _Recap_ - Prinzipien des Requirement Engineering

Das IREB benennt im Lehrplan 9 Prinzipen, deren Einhaltung für ein erfolgreiches Requirement Engineering wichtig sind^[[IREB Certified Professional for Requirements Engineering
– Foundation Level – Lehrplan Version 3.1.0, LE 1 S. 12](https://www.ireb.org/content/downloads/2-cpre-foundation-level-syllabus-3-0/cpre_foundationlevel_syllabus_de_v.3.1.pdf)]. Wir haben sie in den vorigen Abschnitten jeweils kennengelernt und nutzen die Prinzipen als eine Art Wiederholung:

> 1. Wertorientierung: Anforderungen sind Mittel zum Zweck, kein Selbstzweck
> 2. Stakeholder: Im RE geht es darum, die Wünsche und Bedürfnisse der Stakeholder zu befriedigen
> 3. Gemeinsames Verständnis: Erfolgreiche Systementwicklung ist ohne eine gemeinsame Basis nicht möglich
> 4. Kontext: Systeme können nicht isoliert verstanden werden
> 5. Problem – Anforderung – Lösung: Ein unausweichlich ineinandergreifendes Tripel
> 6. Validierung: Nicht-validierte Anforderungen sind nutzlos
> 7. Evolution: Sich ändernde Anforderungen sind kein Unfall, sondern der Normalfall
> 8. Innovation: Mehr vom Gleichen ist nicht genug
> 9. Systematische und disziplinierte Arbeit: Wir können im RE nicht darauf verzichten



### Leitfragen

- Welche Phasen der Anforderungsanalyse gibt es?

- Welche Möglichkeiten der Anforderungsermittlung gibt es? ("Woher kommen die Anforderungen?")

- Welche Stakeholder können Anforderungen an das Projekt stellen?

- In welchen Dokumenten / Artefakten können Anforderungen gesammelt und gemanagt werden?

- Wie ist der generelle Aufbau von Userstories?

- Welche Anforderungen können in einem Use-Case-Diagramm dargestellt werden, welche nicht?

- Was versteht man unter I.N.V.E.S.T.-Kriterien?

- Wie können Anforderungen gegliedert werden?

- Welche Vorteile bietet die Gliederung in Meilensteinen?

- Welche Eigenschaften haben die unterschiedlichen Kategorien der MoSCoW-Methode?

- Welche Eigenschaften haben die unterschiedlichen Kategorien des KANO-Modells?

- Welche Vorteile bietet eine Priorisierung nach Projektrisiko?

- Was versteht man unter einem MVP-Prototypen im Zusammenhang mit Anforderungen?

- Welche Akzeptanzkriterien können für Funktionale Anforderungen und Qualitätsanforderungen formuliert werden?

### Weitere Literatur zu Requirement-Engineering


- Das deutschsprachige Handbuch des IREB (Martin Glinz, Hans van Loenhoud, Stefan Staal, Stan Bühne: Handbuch für das CPRE Foundation Level nach dem IREB-Standard) [findet sich hier als PDF](https://www.ireb.org/de/downloads/#cpre-foundation-level-handbook)

- Das Grundlagenbuch (auch zur Zertifizierung) für Anforderungsanalyse ist: Klaus Pohl, Chris Rupp: [Basiswissen Requirements Engineering](https://dpunkt.de/produkt/basiswissen-requirements-engineering/), dPunkt Verlag Heidelberg, ISBN 978-3-86490-814-9

- Weitere Unterlagen des International Requirement Engineering Boards zur Zertifizierung als "Certified Professional for Requirements Engineering Foundation Level": [https://www.ireb.org/de/cpre/foundation/](https://www.ireb.org/de/cpre/foundation/), insbesondere  [Lehrplan und Glossar](https://www.ireb.org/de/downloads/#cpre-foundation-level-handbook)
