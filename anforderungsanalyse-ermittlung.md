## Anforderungsermittlung (_requirement elicitation_)

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/anforderungsanalyse-ermittlung</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109422853120283884</span>

> **tl/dr;** _(ca. 11 min Lesezeit): Welche Techniken gibt es, um Anforderungen zu ermitteln, dokumentieren, gliedern, validieren und messbar zu machen? Welche Kriterien werden an gute fachgerechte und überprüfbare Anforderungen angelegt?_

Dieser Artikel ist Bestandteil einer mehrteiligen Reihe zu _Requirements Engineering_:

- Grundlagen Anforderungsanalyse: [Wozu dient Anforderungsanalyse und welche Anforderungsarten und Qualitätsmerkmale gibt es?](https://oer-informatik.de/anforderungsanalyse)

- (**dieser Artikel**) Haupttätigkeit Ermittlung: [Woher kommen Anforderungen und mit welchen Techniken ermittle ich sie systematisch?](https://oer-informatik.de/anforderungsanalyse-ermittlung)

- Haupttätigkeit Dokumentation: [Auf welche Arten können Anforderungen dokumentiert werden?](https://oer-informatik.de/anforderungsanalyse-dokumentieren)

- Haupttätigkeiten Validierung und Verwaltung: [Wie kann die Qualität der Anforderungen überprüft werden, wie priorisiere ich sie, und wie  verwalte ich sie im Projektverlauf?](https://oer-informatik.de/anforderungsanalyse-validieren-verwalten)

- [Übungsaufgaben zur Anforderungsanalyse](https://oer-informatik.de/anforderungsanalyse-uebungsaufgaben)

### Haupttätigkeit 1: Anforderungsermittlung (requirements elicitation)

Woher kommen die Anforderungen an das System, mit welchen Techniken kann ich Anforderungen erheben und ermitteln? Durch systematisches Vorgehen können Anforderungen gewonnen, detailliert und verfeinert werden. Auch Konflikte und Abhängigkeiten zwischen Anforderungen können so früh entdeckt werden.

Dabei wird bei größeren Projekten in einem Dreischritt vorgegangen:

* Analyse von Umfeld und Systemkontext

* Analyse der Stakeholder

* Analyse der Anforderungen

Diese Aktivitäten folgen einander, jedoch ergeben sich häufig Rücksprünge, in der Phase des Anforderungsmanagements behandelt werden müssen: neue Anforderungen bringen einen neuen Stakeholder zu Tage, Systemgrenzen werden verschoben usw. Die wesentlichen Aktiviäten sind in folgendem Aktivitätsdiagramm zusammengefasst:

![Aktivitäten der Anforderungsermittlung](plantuml/anforderungsermittlung.png)

#### Projektumfeld und Systemkontext

Projekte finden nicht im luftleeren Raum statt, die zu schaffenden Systeme sollen mit ihrem Umfeld interagieren oder vom Umfeld genutzt werden. Daher gilt es zunächst, das Umfeld zu analysieren, um herauszufinden, von welchen Seiten Anforderungen an ein System gestellt werden können.

Je nach Projektumfeld und _Stakeholder_ sollten dabei möglichst mehrere Techniken und unterschiedliche Gruppen einbezogen werden.

##### Umfeldanalyse

Ein systematischer Weg, das Umfeld zu analysieren, ist das Befüllen eines Umfeldportfolios mit sachlichen und sozialen Umfeldfaktoren sowie nach Faktoren innerhalb des Projekts (direkt/intern) und außerhalb des Projekts (mittelbar/extern).^[Diese Systematik wurde analog zu "Weniger schlecht Projekte managen" erstellt - Quelle siehe Literaturlinks]

||interne Faktoren|externe Faktoren|
|---|---|---|
|**sachliche Faktoren**<br/>Kommunikation<br/>nicht möglich|Prozessbeschreibungen<br/>interne Dokumente (QM-Handbuch, Projekthandbuch...)<br/>Definition of Done<br/>Coding Conventions<br/>IT-Infrastruktur<br/>bestehende Produkte<br/>bestehende Verträge<br/>...|Gesetze<br/>Verordnungen<br/>Normen<br/>Lizenzen<br/>Wetter<br/>...|
|**Soziale Faktoren**<br/>Kommunikation<br/>möglich|Datenschutzbeauftragte<br/>Entwickler\*innen<br/>QS-Team<br/>Auftraggeber\*innen<br/>Marketing<br/>Betriebsrat<br/>Vertrieb<br/>Softwarearchitekt<br/>...|Auftraggeber*innen<br/>Lieferanten<br/>Kund\*innen<br/>Anwender\*innen<br/>Kontrollorganisationen<br/>Öffentlichkeit<br/>NGOs<br/>Nutzer*innen eines Alt-/Konkurrenzprodukts<br/>...|

Bei den sachlichen und sozialen Faktoren lohnt ein Blick auf die Randbedingungen - häufig finden sich zu jeder Rubrik Umfeldfaktoren. Zum Teil sind die Umfeldfaktoren unmittelbar Stakeholder für das Projekt, zum Teil leiten sich Stakeholder aus Umfeldfaktoren ab. Aber was sind eigentlich _Stakeholder_?

##### Systemkontextabgrenzung

Mit der Umfeldanalyse sollte in weiten Teilen die Schnittstellen unseres Systems festgelegt sein. Wichtig ist es im nächsten Schritt, sich zu verdeutlichen, welche Faktoren Bestandteil des Systems selbst sind, welche im Systemkontext liegen (also im für das Projekt relevanten Teil des Umfelds) und welche für das Projekt irrelevant sind.

Sämtliche Anforderungen, die das System nach Abschluss des Projekts erfüllen soll, befinden sich innerhalb der Systemgrenze. Die betrachtete Umgebung wird nach außen hin durch die Kontextgrenze abgegrenzt. Sie legt fest, welcher Teil der Umwelt für das Projekt irrelevant ist. Innerhalb des Systemkontexts befinden sich die Stakeholder, interagierende Systeme, Randbedingungen, sowie (Geschäfts-)Prozesse, die Einfluss auf das System haben. Diese interagieren über definierte Schnittstellen über die Systemgrenze mit dem System selbst.

![Abgrenzung zwischen irrelevanter Umgebung, Systemkontext (inkl. Stakeholer, Randbedingungen, interagierende Systeme, Prozesse) und System](images/systemkontext.jpg)

Durch fehlende Informationen und Erfahrungen, neue Erkenntnisse oder sich verändernde Bedingungen können sich an den Rändern von System- und Kontextgrenze Grauzonen befinden. Zudem verschieben sich die Grenzen Projektverlauf:  Einige Anforderungen, die zunächst eingeplant wurden, können vielleicht nicht umgesetzt werden (andere werden zusätzlich umgesetzt). Das verschiebt die Systemgrenze. Oft wird auch erst im Projektverlauf klar, welche Randbedingung und welche Stakeholder tatsächlich für das Projekt relevant sind. Hierdurch kann sich die Kontextgrenze verschieben.

Das Verständnis des Kontexts ist so wichtig, dass das IREB das 4. Prinzip des Requirement-Engineerings benannt hat:

> Prinzip 4: Kontext - Systeme können nicht isoliert betrachtet werden.

##### Stakeholdermanagement


Wer sind Stakeholder? Das IREB-Glossar definiert das so:^[Zitat aus dem [IREB-Glossar](https://www.ireb.org/content/downloads/1-cpre-glossary-2-0/ireb_cpre_glossary_en_2.0.1.pdf)]

> A person or organization who influences a system’s requirements or who is impacted by that system.
> Note: Influence can also be indirect. For example, some stakeholders may have to follow instructions issued by their managers or organizations.

Stakeholder stellen auf der einen Seite Anforderungen an das System, auf der anderen Seite haben Sie auch Erwartungen oder Befürchtungen zum Projekt. Sie verfolgen eigene Ziele, für den Projekterfolg ist daher wichtig, dass dem Umgang mit Stakeholdern einem eigenen Management-Kreis (Plan-Do-Arc-Check) zugeteilt wird. Schön dargestellt ist dies z.B. im Lehrbuch zu "Projektmanagement (IPMA)" von Dittmann/Dirbanis, deren Systematik ich hier übernehme:

Im ersten Schritt müssen die _Stakeholder_ identifiziert werden. Stakeholder finden sich durch die Umfeldanalyse (siehe oben), Checklisten, Organigramme, Geschäftsprozessdokumentationen, Stakeholderbefragung zu weiteren Stakeholdern.

Im zweiten Schritt werden die Befürchtungen und Erwartungen der Stakeholder an das System zusammengetragen. Hieraus lassen sich später Ziele der Stakeholder ableiten.

Im dritten Schritt werden die Stakeholder kategorisiert nach Macht/Einfluss und Konfliktpotenzial. Das wird zunächst tabellarisch zusammengetragen:

|Nr|Name Stakeholder<br/>Stakeholdergruppe|Kontaktdaten und Erreichbarkeit|Fachgebiet und Umfang des Fachwissens|Ziele und Interessen:<br/>Risiko aus Sicht der Stakeholder<br/>(positives Risiko: Erwartungen / negatives Risiko: Befürchtungen)|Relevanz (Beschreibung)|Macht/Einfluss (wenig/mittel/viel)|Konfliktpotenzial (wenig/mittel/viel)|Geplante Maßnahmen zur Einbindung / Eindämmung des SH
|---|---|---|---|---|---|---|---|---|
|---|---|---|---|---|---|---|---|---|

Leichter auswertbar ist diese Tabelle, wenn wir Macht/Einfluss und Konfliktpotenzial in drei Gruppen (viel/mittel/wenig oder 3/2/1) aufteilen und in Form einer Stakeholdermatrix auftragen:

||wenig<br/>Einfluss|mittel<br/>Einfluss|viel<br/>Einfluss|
|---|---|---|---|
|**wenig**<br/>**Konfliktpotenzial**|||
|**mittel<br/>Konfliktpotenzial**|||
|**viel<br/>Konfliktpotenzial**|||

Je nach Anordnung im Portfolio sollte in der Kommunikation mit den Stakeholdern eine angepasste Strategie angewendet werden. Hierzu dient der vierte Schritt: die Maßnahmenplanung.

Besonderes Augenmerk liegt dabei auf Stakeholder mit viel Einfluss und hohem Konfliktpotenzial. Sie können ein Projekt torpedieren - Ziel muss es sein, das Konfliktpotenzial dieser Stakeholdergruppe zu minimieren. Gute Anhaltspunkte für Maßnahmen des Stakeholdermanamgents finden sich z.B. in "Weniger schlecht Projekte managen" (siehe Quellen unten).

Und im fünften Schritt wird die Wirksamkeit der Maßnahmenplanung reflektiert - wie in allen Managementprozessen darf auch beim Stakeholdermanagement die Evaluierungsphase nicht fehlen. Waren die getroffenen Maßnahmen zur Einbindung / Eindämmung der Stakeholder erfolgreich? War die Kommunikation in Art, Qualität und Quantität zielführend? Welche Korrekturen am Stakeholderportfolio und der Stakeholdertabelle müssen umgesetzt werden?

Das zweite Prinzip des Requirement Engineering nach dem IREB sind Stakeholder: Im Requirement Engineering geht es darum, die Wünsche und Bedürfnisse der Stakeholder zu befriedigen.

#### Anforderungsermittlung

Mit der kategorisierten Liste an Stakeholdern kann es nun zur eigentlichen Hauptaufgabe kommen: die Anforderungen ermitteln.

Wir erhalten die Anforderungen von Stakeholdern, aus Dokumenten oder von Systemen. Hierbei helfen eine Reihe von Techniken:

* Befragungstechniken
  * Interview: Ein offenes Gespräch ohne festgelegten geschlossenen Fragenkatalog mit unterschiedlichen _Stakeholdern_

  * Umfrage (zuvor definierte Fragen) mit unterschiedlichen _Stakeholdern_

* Kreativtechniken für Workshop / Meeting / Brainstorming mit Stakeholdern

    * Perspektivwechsel

    * Analogietechniken: anhand analoger/ähnliche Situationen Anforderungen ableiten

    * Vorstellung von Prototypen

    * Storytelling: Emotionalisierung

    * Design Thinking

    * Storyboard: visuelle Vorlagen zum Entwurf von Abläufen (Drehbuch)

    * Personas, um einzelne Stakeholdergruppen im Blick zu haben (siehe auch Perspektivwechseltechnik)

* Workshop / Meeting / Brainstorming / Rollenspiele mit

  * Entwicklerteam

  * Mitarbeitern der Fachdomäne

  * anderen Stakeholdern

* Analyse der bestehenden Geschäftsprozesse über:

  * Anwender-Beobachtung

  * Analyse der Dokumentation von Geschäftsprozessen (z.B. Dokumente aus dem Qualitätsmanagement beteiligter Unternehmen, EPK, Flussdiagramme)

  * Analyse bestehender Altsysteme

* Marktanalyse:

  * Gibt es bereits Konkurrenzprodukte? Dann können diese direkt analysiert werden

  * In welchen Bereichen gibt es vergleichbare / ähnliche Geschäftsprozesse, die analysiert werden können?

* Auswertung bestehender Dokumente, z.B.
     - Normen:

       - Usability (ISO 9231 -11): Erfahrungen während der Nutzung
       - User Experience (ISO 9231 -210): Erfahrungen vor, während und nach der Nutzung

     - Gesetze

Wichtig ist bei der Anforderungsermittlung insbesondere, dass der Blick aufs Ganze erweitert wurde:

* Wer sind die _Stakeholder_ (also diejenigen, die ein Interesse an dem System haben oder von diesem betroffen sind)?
Wer ist in die Geschäftsprozesse eingebunden: wer sind die Auftraggeber, Kunden, Anwender, Mitarbeiter der Fachdomäne, Kontrollorganisationen, Verantwortliche, Ausführende usw. ?

* Welche Ziele verfolgen diese _Stakeholder_ mit dem System? In welche Szenarien nutzen Sie das System? Welche Anforderungen ergeben sich daraus?

* Haben alle Projektbeteiligten ein gemeinsames Verständnis des Systems, haben sie eine gemeinsame Sprache / ein einheitliches Vokabular und meinen alle mit jedem Begriff das gleiche?

* Welche konkurrierenden / widersprüchlichen Anforderungen gibt es?

* Wurde das System im Gesamtkontext mit den umgebenden anderen Systemen betrachtet?

* Welche Randbedingungen werden angenommen (muss zusätzlich erfasst werden!)? Randbedingungen können z.B. technischer, rechtlicher, kultureller, organisatorischer, ökologischer oder physikalischer Natur sein (keine abschließende Liste!).

* Welche realistischen Änderungen der Randbedingungen müssen berücksichtigt werden?

### Wie geht es weiter?

Die Anforderungen liegen auf dem Tisch. Im folgenden Blogpost wird es darum gehen, ...

[... die Anforderungen zu dokumentieren (_requirement documentation_)](https://oer-informatik.de/anforderungsanalyse-dokumentieren)

### Leitfragen

- Welche Möglichkeiten der Anforderungsermittlung gibt es? ("Woher kommen die Anforderungen?")

- Welche Stakeholder können Anforderungen an das Projekt stellen?

### Weitere Literatur zu Requirement-Engineering

- Das deutschsprachige Handbuch des IREB (Martin Glinz, Hans van Loenhoud, Stefan Staal, Stan Bühne: Handbuch für das CPRE Foundation Level nach dem IREB-Standard) [findet sich hier als PDF](https://www.ireb.org/de/downloads/#cpre-foundation-level-handbook)

- Das Grundlagenbuch (auch zur Zertifizierung) für Anforderungsanalyse ist: Klaus Pohl, Chris Rupp: [Basiswissen Requirements Engineering](https://dpunkt.de/produkt/basiswissen-requirements-engineering/), dPunkt Verlag Heidelberg, ISBN 978-3-86490-814-9

- Weitere Unterlagen des International Requirement Engineering Boards zur Zertifizierung als "Certified Professional for Requirements Engineering Foundation Level": [https://www.ireb.org/de/cpre/foundation/](https://www.ireb.org/de/cpre/foundation/), insbesondere  [Lehrplan und Glossar](https://www.ireb.org/de/downloads/#cpre-foundation-level-handbook)

- Karen Dittmann / Konstantin Dirbanis: [Projektmanagement (IPMA) - Lehrbuch für Level D und Basiszertifikat (GPM)](https://shop.haufe.de/prod/projektmanagement-lehrbuch), Haufe Verlag Freiburg, 1. Auflage 2020 ISBN 978-3-648-14736-8

- Anne Schüßler / Peter Schüßler: [Weniger schlecht Projekte managen](https://oreilly.de/produkt/weniger-schlecht-projekte-managen/), O'Reilly / dPunkt Verlag Heidelberg, 1. Auflage 2020 ISBN 978-3-96009-14-4
