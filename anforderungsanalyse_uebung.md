## Anforderungsanalyse-Übungsaufgabe (_requirement engineering_)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/anforderungsanalyse-uebungsaufgaben</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/112724316650421938</span>

> **tl/dr;** _(ca. 45 min Bearbeitungszeit): Die Probleme vieler Projekte basieren auf unzureichender oder nicht fachgerechter Anforderungsanalyse. In diesem Artikel sind einige Übungsaufgaben zu Techniken und Best-Practices der Anforderungsanalyse (Lernfeld 12, Fachinformatik) gesammelt._

Dieser Artikel ist Bestandteil einer mehrteiligen Reihe zu _Requirements Engineering_:

- Grundlagen Anforderungsanalyse: [Wozu dient Anforderungsanalyse und welche Anforderungsarten und Qualitätsmerkmale gibt es?](https://oer-informatik.de/anforderungsanalyse)

- Haupttätigkeit Ermittlung: [Woher kommen Anforderungen und mit welchen Techniken ermittle ich sie systematisch?](https://oer-informatik.de/anforderungsanalyse-ermittlung)

- Haupttätigkeit Dokumentation: [Auf welche Arten können Anforderungen dokumentiert werden?](https://oer-informatik.de/anforderungsanalyse-dokumentieren)

- Haupttätigkeiten Validierung und Verwaltung: [Wie kann die Qualität der Anforderungen überprüft werden, wie priorisiere ich sie, und wie  verwalte ich sie im Projektverlauf?](https://oer-informatik.de/anforderungsanalyse-validieren-verwalten)

- (**dieser Artikel**) [Übungsaufgaben zur Anforderungsanalyse](https://oer-informatik.de/anforderungsanalyse-uebungsaufgaben)

#### Übungsaufgabe Anforderungsanalyse Servermigration (Systemintegration)

In einem Projekt soll eine _on premises_ Serveranwendung zur Arbeitszeiterfassung (bestehend aus einer Webapp und einer Datenbank) in zwei Container in die Cloud migriert werden.

a) Erstelle hierfür eine tabellarische Umfeldanalyse (über 4 Quadranten). Benennen für jeden Quadranten zwei Umfeldfaktoren.

b) Erstelle hierfür eine Stakeholderanalyse mit 6 relevanten Stakeholdern.

c) Nenne 4 verschiedene Techniken, mithilfe derer Anforderungen an das Projekt ermittelt werden können.

d) Nenne jeweils 3 funktionale und nicht-funktionale Anforderungen an das Projekt.

e) Dokumentiere die Anforderungen tabellarisch mit allen relevanten Informationen zum Zeit- und Qualitätsmanagement.

f) In welchen Dokumenten / Artefakten können Anforderungen gesammelt und gemanagt werden?

g) Was versteht man unter I.N.V.E.S.T.-Kriterien? Beschreibe für eine Anforderung, wie sie den obigen Kriterien genügt.

h) Wie können Anforderungen gegliedert/priorisiert werden? Nenne zwei Techniken und weise die oben genannten Anforderungen den Kategorien zu!

i) Welche Vorteile bietet die Gliederung in Meilensteinen?

j) Welche Akzeptanzkriterien können für funktionale Anforderungen und Qualitätsanforderungen formuliert werden?

k) Nenne die Qualitätskriterien, für die das F.U.R.P.S.-Akronym steht!

l) Nenne vier Qualitätskriterien, die in der Software-Qualitätsnorm ISO 25000 festgeschrieben sind.

### Requirement Engineering

Das Requirement Engineering gliedert sich in vier Phasen. Nenne jede Phase und beschreibe stichpunktartig die Haupttätigkeit, die innerhalb dieser Phase durchgeführt wird. 	(____P von 8P) 

1. <button onclick="toggleAnswer('rce1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="rce1">

Elicitation (Ermittlung):

Sammlung der Anforderungen durch systematische Analyse des Umfelds, der Stakeholder und Nutzung von Ermittlungstechniken.

</span>

2. <button onclick="toggleAnswer('rce2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="rce2">

Documentation:

Niederschrift der Anforderungen mit allen relevanten Informationen in einer Tabelle, einem Flusstext oder einem Diagramm

</span>


3.  <button onclick="toggleAnswer('rce3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="rce3">

Validation:

Erstellung eines gemeinsamen Verständnisses, Konfliklösung zwischen Anforderungen, Prüfung, ob Anforderungen wertvoll und in ihrer Güte hinreichend sind und Gliederung/Priorisierung

</span>


4.  <button onclick="toggleAnswer('rce4')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="rce4">

Management:

PDCA-Zyklus für Anforderungen während des gesamten Projekts: Beständiges Prüfen, ob die Anforderungen und deren Priorisierung noch sinnvoll sind.

</span>

### Multiple-Choice / Single-Choice-Fragen

Welche Aussagen treffen zu?

|Aussage|trifft zu?|
|---|---|
|Über die Umfeldanalyse werden Stakeholder identifiziert, die mit besonders großem Einfluss und Konfliktpotenzial das Projekt gefährden können.<button onclick="toggleAnswer('mc1')">show</button>|<span class="hidden-answer" id="mc1">Nein, das passiert bei der Stakeholderanalyse, nicht bei der Umfeldanalyse!</span>|
|MVPs stellen im iterativ-inkrementellen Vorgehen kleinstmögliche Produkte dar, die bereits Nutzen generieren. <button onclick="toggleAnswer('mc2')">show</button>|<span class="hidden-answer" id="mc2">Ja, trifft zu.</span>|
|Man beschreibt die Qualität von Anforderungsformulierungen mit der Abkürzung I.N.V.E.S.T. Das „I“ steht für informativ. <button onclick="toggleAnswer('mc3')">show</button>|<span class="hidden-answer" id="mc3">Nein, das I steht für Independent (unabhängig).</span>|
|Nach dem KANO-Modell können Anforderungen der Kategorie „Won’t have“ zugeordnet werden. <button onclick="toggleAnswer('mc4')">show</button>|<span class="hidden-answer" id="mc4">Nein, _Won't have_ sind nach MosCoW die Anforderungen, die erst in einem nächsten Entwicklungsschritt geplant sind.</span>|
|Es gibt in der MoSCoW-Methode eine Kategorie mit Anforderungen, deren Umsetzung zu Unzufriedenheit beim Kunden führen würde. <button onclick="toggleAnswer('mc5')">show</button>|<span class="hidden-answer" id="mc5">Nein, so eine Kategorie gibt es nur bei KANO: die Rückweisungsmerkmale.</span>|


Was ist Bestandteil eines (tabellarisch) ausgearbeiteten Anwendungsfalls (UseCase)? Zutreffendes bitte ankreuzen, es können mehrere oder keine Antwort zutreffen.	(____P von 2P)

|Eigenschaft|Bestandteil eines Anwendungsfalls?|
|---|---|
|Nennung eines Hauptszenarios <button onclick="toggleAnswer('mc2-1')">show</button>|<span class="hidden-answer" id="mc2-1">Ja, das ist die Kernkomponente eines UseCase.</span>|
|Nennung eines oder mehrerer Nebenszenarien. <button onclick="toggleAnswer('mc2-2')">show</button>|<span class="hidden-answer" id="mc2-2">Ja, häufig gibt es viele Szenarien, die bei einem UseCase möglich sind.</span>|
|Nennung von Fehlerzuständen <button onclick="toggleAnswer('mc2-3')">show</button>|<span class="hidden-answer" id="mc2-3">Nicht nur der Happy-Path ist Bestandteil, auch Szenarien, die zu Fehlerzuständen führen.</span>|
|Nennung aller betroffener Stakeholder <button onclick="toggleAnswer('mc2-4')">show</button>|<span class="hidden-answer" id="mc2-4">Akteure werden genannt, Stakeholder jedoch normalerweise nicht - schon gar nicht _alle_.</span>| 

### Tabellarische Anforderungsdokumentation

Es soll eine App erstellt werden, in der IHK-Prüfungsfragen in einem hierarchischen Themenbaum gegliedert werden, um Aufgaben zu bestimmten Themen gezielt suchen zu können.

Der Kunde nennt als Anforderung: "Fehlerhafte Einträge im Themenbaum sollen intuitiv gelöscht werden können, wobei versehentliche Löschvorgänge vermieden werden sollen."

Stelle eine beispielhafte daraus resultierenden Anforderung exemplarisch fachgerecht in einer Tabelle dar und nenne alle für Qualitäts- und Zeitmanagement erforderlichen Attribute.	 (____P von 15P)  <button onclick="toggleAnswer('tbuc1')">show</button>

<span class="hidden-answer" id="tbuc1">

|Nr.|Name der Anforderung|Beschreibung des Nutzens und der Realisierung (z.B. als Satzschablone)|Akzeptanzkriterium<br/> (wann gilt Anforderung als erfüllt)|Grundlegende Testfälle<br/>(Dokumentation von Eingabe, Randbedingung, erwartetem Ergebnis an anderer Stelle)|Priorisierung / <br/>Risiko bei Nichterfüllung|Geschätzter Aufwand|
|---|---|---|---|---|---|---
|1.|Löschen|Als Nutzer möchte ich Einträge aus dem Themebaum löschen können, damit der Themenbaum fehlerfrei bleibt.|Leere Themenbaumknoten werden gelöscht, Knoten mit Unterelementen werden nicht gelöscht.|(1) Root-Knoten löschen => Abbruch<br/>(1) Eltern-Knoten löschen: Abbruch<br/>(3) Blattknoten löschen => Ausführung<br/>|Must-have|Inkonsistenter Themenbaum mit fehlenden Einträgen führt zu Unzufriedenheit beim Kunden|1h<br/>(oder xy Storypoints)|
|2.|Intuitives Löschen|Als Nutzer möchte ich über bekannte Symbole (wie z.B. ein Trash-Can-Symbol) darauf hingewiesen werden, wie/wo ich fehlerhafte Einträge löschen kann, damit ich nicht lange recherchieren muss, wie das geht.|10 Versuchspersonen, die bisher nicht mit diesem Programm gearbeitet haben, wird ein Screenshot der Anwendung gezeigt. Alle zehn müssen erkennen, wie das Löschen initiiert wird.|(siehe Akzeptanztest)|Must-have|Unzufriedenheit der Anwender|1h<br/>(oder xy Storypoints)|
|3.|Vermeidung<br/>versehentlicher<br/>Löschvorgänge|Als Nutzer möchte ich vor dem endgültigen Löschen nochmals gefragt werden, damit ich die Möglichkeit habe, versehentliches Löschen zu verhindern.|Nach Betätigen des Lösch-Buttons wird in einem Pop-Up-Fenster nochmals um Bestätigung gebeten. Wird dies nicht bejaht, wird der Löschvorgang abgebrochen.|Löschen – Bestätigung bejaht – Daten weg.<br/>Löschen – Bestätigung verneint – Daten da|Must-have|Ausschlusskriterium, es droht Datenverlust|1h<br/>(oder xy Storypoints)|

</span>

### Kriterien guter Anforderungsformulierungen

Werden Anforderungen auf Task-Karten gegliedert, so beschreibt man die Qualität von Anforderungsformulierungen mit der Abkürzung I.N.V.E.S.T. Wofür steht diese Abkürzung? Nenne für die Anforderungen aus der vorigen Aufgabe Beispiele, wie sie dem Qualitätskriterium genügen. (____P von 6P)

|	|…steht für... Beispiel|
|---|---|---| 
|I.<button onclick="toggleAnswer('invI')">show</button>|<span class="hidden-answer" id="invI">Independent: Die Anforderung, Einträge löschen zu können ist unabhängig von anderen Anforderungen realisierbar.</span>|	
|N.<button onclick="toggleAnswer('invN')">show</button>|<span class="hidden-answer" id="invN">Negotiable: Falls sich später für einen komplexeren Nachfrage- und Sicherheitsmechanismus entschieden wird, ist das verhandelbar.</span>|	
|V.<button onclick="toggleAnswer('invV')">show</button>|<span class="hidden-answer" id="invV">Valuable: Die Tatsache, dass ich Einträge im Themenbaum löschen kann, stellt einen Mehrwert für den Benutzer\*innen/Administrator\*innen dar.</span>|	
|E.<button onclick="toggleAnswer('invE')">show</button>|<span class="hidden-answer" id="invE">Estimateable: Die Anforderung ist hinreichend gut bekannt, dass es möglich ist, den Aufwand zu schätzen.</span>|	
|S.<button onclick="toggleAnswer('invS')">show</button>|<span class="hidden-answer" id="invS">Small: Die Anforderung ist klein genug, um sie innerhalb eines Sprints zu realisieren.</span>|
|T.<button onclick="toggleAnswer('invT')">show</button>|<span class="hidden-answer" id="invT">Testable: Die Anforderungen verfügen über Akzeptanzkriterien, mit denen der erfolgreiche Abschluss der Anforderung nachgewiesen werden kann.</span>|	

