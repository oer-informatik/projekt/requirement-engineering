## Dokumentieren von Anforderungen

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/anforderungsanalyse-dokumentieren</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109422853120283884</span>

> **tl/dr;** _(ca. 11 min Lesezeit): Es stehen die Wünsche der Stakeholder im Raum - und auch die Normen und Gesetze wollen als Anforderung dokumentiert werden. Welche unterschiedlichen Artefakte gibt es, in denen Anforderungen erfasst werden können, und wo eignen sie sich besonders?_

Dieser Artikel ist Bestandteil einer mehrteiligen Reihe zu _Requirements Engineering_:

- Grundlagen Anforderungsanalyse: [Wozu dient Anforderungsanalyse und welche Anforderungsarten und Qualitätsmerkmale gibt es?](https://oer-informatik.de/anforderungsanalyse)

- Haupttätigkeit Ermittlung: [Woher kommen Anforderungen und mit welchen Techniken ermittle ich sie systematisch?](https://oer-informatik.de/anforderungsanalyse-ermittlung)

- (**dieser Artikel**) Haupttätigkeit Dokumentation: [Auf welche Arten können Anforderungen dokumentiert werden?](https://oer-informatik.de/anforderungsanalyse-dokumentieren)

- Haupttätigkeiten Validierung und Verwaltung: [Wie kann die Qualität der Anforderungen überprüft werden, wie priorisiere ich sie, und wie  verwalte ich sie im Projektverlauf?](https://oer-informatik.de/anforderungsanalyse-validieren-verwalten)

- [Übungsaufgaben zur Anforderungsanalyse](https://oer-informatik.de/anforderungsanalyse-uebungsaufgaben)

### Haupttätigkeit 2: Dokumentieren der Anforderungen (_requirements documentation_)

Je nach Vorgehensmodell und Granularität (Detailgrad) der erfassten Anforderungen können sich die Dokumentationsmittel unterscheiden.

#### Übersicht von Anforderungen

Anforderungen können auf verschiedenste Art dokumentiert werden. Wichtig ist hierbei, dass die wesentlichen Kriterien (siehe unten) genannt sind. In der folgenden Phase wird die Qualität der so erfassten Anforderungen überprüft.

Um das _big picture_ von Anforderungen zu erhalten bieten sich beispielsweise folgende Artefakte an:

##### Anforderungsdokumente (Lastenheft / Pflichtenheft)

Wenn Anforderungen von Beginn an relativ fest stehen können oft textuelle Auflistung der Anforderungen vorab erfolgen:

  *   In diesen Anforderungsdokumenten können funktionale und nicht-funktionale Anforderungen festgeschrieben werden. Der Grad an Formalität kann von Stichpunktlisten bis hin zu streng formellen Dokumenten reichen.

  * Das **Lastenheft** wird durch Auftraggeber erstellt: Was soll umgesetzt werden und warum? Es enthält oft noch keine technische Sicht und ist in der Sprache der Problemdomäne formuliert. ([Hier ein Beispiel für ein Microcontrollerprojekt](https://oer-informatik.de/wasserfall_mcu_projekt_lastenheft))

  * Im **Pflichtenheft** erarbeitet der Auftragnehmer wie und womit die Anforderungen des Lastenhefts umgesetzt werden sollen. Es beschreibt Werkzeuge und (grobe) Entwürfe und nutzt die Sprache der Lösungsdomäne. Das Pflichtenheft wird i.d.R. Vertragsbestandteil und sollte mit entsprechender Sorgfalt erstellt werden. ([Hier ein Beispiel für ein Microcontrollerprojekt](https://oer-informatik.de/wasserfall_mcu_projekt_pflichtenheft))

Pflichtenhefte können auch in Stichpunkten oder tabellarisch ausgeführt sein. Beispielhaft könnten folgende Aspekte erfasst werden:

|Nr. | Name der Anforderung | Beschreibung des Nutzens und der Realisierung<br/>(z.B. als Satzschablone) | Akzeptanzkriterium<br/> (wann gilt Anforderung als erfüllt) | Grundlegende Testfälle<br/> (Dokumentation von Eingabe, Randbedingung, erwartetem Ergebnis an anderer Stelle) | Priorisierung / Risiko<br/> bei Nichterfüllung | Geschätzter Aufwand|
|--- | --- | --- |--- | --- | --- |--- |
|1. | Einschalten | Das System muss einfach per Taster einschaltbar sein, um mit der Benutzung starten zu können. | Betätigen eines Knopfs führt innerhalb von 10s in das Startmenü mit aktivierter Anzeige.| 1. Einschalten wenn aus: an<br/>2. Einschalten wenn an: Meldung<br/>3. Einschalten während Hochfahren: Meldung<br/>4. Einschalten während Runterfahren: Meldung | _must-have_<br/><br/>Ausschlusskriterium |2h|


##### Anwendungsfälle (Use-Cases)

Anwendungsfälle können [tabellarisch](https://oer-informatik.de/anwendungsfall) oder als Übersicht per [UML-Anwendungsfalldiagramm](https://oer-informatik.de/uml-usecase) erfasst werden. 

  * "A use case is all the ways of using a system to achieve a particular goal for a particular user. Taken together the set of all the use cases gives you all of the useful ways to use the system, and illustrates the value that it will provide." (Ivar Jacobson) ^[Ivar Jacobson, der Initiator der UseCase-Diagramme, in "Use-Case 2.0": https://www.ivarjacobson.com/sites/default/files/field_iji_file/article/use-case_2_0_jan11.pdf]
  * Anwendungsfälle können in **[UML _Use-Case_-Diagrammen](https://oer-informatik.gitlab.io/uml/umlanwendungsfall/uml-usecase.html)** (Anwendungsfalldiagramm) graphisch und übersichtlich dargestellt werden

  ![UML Use-Case-Diagramm](images/use-case-example.png)

  * Mithilfe dieser einfachen grafischen Darstellungsform können Anwendungsfälle dialogisch mit _Stakeholdern_ erarbeitet werden.

  * In Anwendungsfällen werden funktionale Anforderungen zusammengefasst, die gemeinsam von einem Akteur initiiert werden und dem Anwender einen Nutzen generieren

  * **Use-Case-Diagramme sind jedoch nur eine Übersicht, Anwendungsfälle haben weit mehr Details!** Die einzelnen Anwendungsfälle können im nächsten Schritt [tabellarisch präzisiert](https://oer-informatik.de/anwendungsfall) werden. Hierbei wird ein Name, eine kurze Beschreibung, ein auslösender Akteur, Randbedingungen sowie Haupt- und Nebenszenarien erfasst.

  * Einzelne Wege durch den Anwendungsfall werden als Szenarien definiert und als Stichpunktliste oder ggf. als UML Aktivitäts-, Zustands- oder Sequenzdiagramm entworfen.

##### Product Backlog Items

Vor allem im agilen Umfeld werden die Anforderungen häufig als einzelne isolierte Tasks gesammelt. Diese Tasks sollten - unabhängig von der genutzten Technik - so formuliert werden, dass sie den I.N.V.E.S.T.-Kriterien genügen:

|Name |Beschreibung |
|:---|:---|
|I:Independent|Jeder Task muss unabhängig von anderen Tasks umgesetzt werden können. Er sollte frei von Abhängigkeiten sein.|
|N:Negotiable|Nichts ist in Stein gemeißelt. Der Inhalt eines Tasks ist im Team verhandelbar: sie können geteilt, geändert, zusammengelegt... werden.|
|V:Valueable|Jeder Task produziert aus sich heraus einen Nutzen und hat einen Wert für den Anwender.|
|E:Estimatable|Das Team muss in der Lage sein, abschätzen zu können, wie lange es für jeden Task etwa benötigt. Erscheint die Schätzung nicht möglich ist der Task vermutlich zu groß.|
|S:Small|Tasks müssen die richtige Größe haben: zu große Tasks sollten unterteilt werden. Ziel muss es sein, dass jeder Task am Ende eines Tages in das Repository übernommen werden kann.|
|T:Testable|Alle Daten, die zum Testen des Tasks nötig sind, müssen genannt sein. Im einfachsten Fall sind dies Akzeptanzkriterien.|


##### Userstories / Satzschablonen

Es kann hilfreich sein zur Formulierung der Anforderungen auf Satzschablonen zuzugreifen, um die Formulierungen einfach und präzise zu halten. Die bekannteste Satzschablone stellen die **User Stories** dar, die festlegen, dass Rolle des Nutzers, Ziel der Anforderung und der Nutzen für die Anwender genannt werden müssen:

``Als ROLLE möchte ich ZIEL um zu NUTZEN``

Eine weitere Satzschablonen, mit deren Hilfe Anforderungen beschrieben werden können, ist die folgende:

![Satzschablone: Das System muss oder sollte oder wird zukuenftig oder kann zukuenftig <WEM?> die Moeglichkeit bieten oder faehig sein OBJEKT & ERGAENZUNG PROZESSWORT.](images/satzschablone.png)


#### Welche Informationen sollten zu Anforderungen erfasst werden?

Im Idealfall wird für jede Anforderung bereits im Vorfeld erfasst:

* Name der Anforderung / des Tasks

* Beschreibung (ggf. in Form von Satzschablonen)

* Akzeptanzkriterium: Messbare und mit Stakeholdern abgestimmte Bedingung, wann eine Anforderung als erfüllt gilt (besonders bei nicht funktionalen Anforderungen wichtig!)

* Grundlegende Testfälle (Eingabeparameter, Randbedingungen, erwartetes Ergebnis)

* Risiko bei Nichterfüllung / fehlerhafter Erfüllung

* Geschätzter Aufwand

#### Festlegung von Akzeptanzkriterien bei Qualitätsanforderungen

Nach der FURPS-Methodik ist die Beschreibung von Akzeptanzkriterien v.a. bei den Qualitäts- (also  nicht funktionalen) Anforderungen schwierig. Es sollten nach Möglichkeit Metriken entwickelt und festgeschrieben werden, die die Erfüllung der Anforderung objektivieren. Ein paar Beispiele als Anhaltspunkt:

* U wie Usability:
Ein Nutzer mit gegebenem Vorwissen muss eine definierte Aufgabe ohne Hilfe in einer definierten Zeitspanne abarbeiten können. Die Zeit wird gestoppt und das Verhalten des Nutzers beobachtet.

* R wie Reliability:
Das System wird mit einem Smoketest bewusst an die Belastungsgrenze gebracht (definierte Anzahl an Zugriffen pro Zeiteinheit). Die akzeptablen Anwortzeiten oder verlorenen Pakete / Neustarts werden festgeschrieben. Informationen zu Abstürzen werden den Logfiles entnommen.

* P wie Performance:
Analog zur Reliability werden für die maximal erwarteten Nutzungsdaten Antwortzeiten des Systems erfasst und als Metrik festgeschrieben.

* S wie Supportablity
Hier kann z.B. festgeschrieben werden, wie lange ein Durchgang durch die CI/CD-Pipeline (also von der IDE bis zum getesteten Code in der Produktivumgebung) dauern darf. Die Update-Zyklendauer, die verpflichtende Benutzung von Clean-Code Metriken (zyklomatische Komplexität, Testabdeckung) geben darüber hinaus ein Maß der Wartbarkeit, dass vereinbart werden kann.


### Wie geht es weiter?

Noch haben wir uns keine Gedanken gemacht, welche Anforderungen wirklich wichtig sind - und woran ich gute Anforderungen erkenne. Darum geht es im nächsten Blogpost zu ...

[... Validierung und Verwaltung von Anforderungen (_requirement validation_)](https://oer-informatik.de/anforderungsanalyse-validieren-verwalten)


### Leitfragen

- In welchen Dokumenten / Artefakten können Anforderungen gesammelt und gemanagt werden?

- Wie ist der generelle Aufbau von Userstories?

- Welche Anforderungen können in einem Use-Case-Diagramm dargestellt werden, welche nicht?

- Was versteht man unter I.N.V.E.S.T.-Kriterien?


### Weitere Literatur zu Requirement-Engineering


- Das deutschsprachige Handbuch des IREB (Martin Glinz, Hans van Loenhoud, Stefan Staal, Stan Bühne: Handbuch für das CPRE Foundation Level nach dem IREB-Standard) [findet sich hier als PDF](https://www.ireb.org/de/downloads/#cpre-foundation-level-handbook)

- Das Grundlagenbuch (auch zur Zertifizierung) für Anforderungsanalyse ist: Klaus Pohl, Chris Rupp: [Basiswissen Requirements Engineering](https://dpunkt.de/produkt/basiswissen-requirements-engineering/), dPunkt Verlag Heidelberg, ISBN 978-3-86490-814-9

- Weitere Unterlagen des International Requirement Engineering Boards zur Zertifizierung als "Certified Professional for Requirements Engineering Foundation Level": [https://www.ireb.org/de/cpre/foundation/](https://www.ireb.org/de/cpre/foundation/), insbesondere  [Lehrplan und Glossar](https://www.ireb.org/de/downloads/#cpre-foundation-level-handbook)

