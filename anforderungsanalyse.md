## Anforderungsanalyse (_requirement engineering_)

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/anforderungsanalyse</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109422853120283884</span>

> **tl/dr;** _(ca. 8 min Lesezeit): Viele Projekte scheitern daran, dass nie hinreichend definiert wurde, was eigentlich die Ziele sein sollen. Wie kann eine systematische Anforderungsanalyse aussehen?_

Dieser Artikel ist Bestandteil einer mehrteiligen Reihe zu _Requirements Engineering_:

- (**dieser Artikel**) Grundlagen Anforderungsanalyse: [Wozu dient Anforderungsanalyse und welche Anforderungsarten und Qualitätsmerkmale gibt es?](https://oer-informatik.de/anforderungsanalyse)

- Haupttätigkeit Ermittlung: [Woher kommen Anforderungen und mit welchen Techniken ermittle ich sie systematisch?](https://oer-informatik.de/anforderungsanalyse-ermittlung)

- Haupttätigkeit Dokumentation: [Auf welche Arten können Anforderungen dokumentiert werden?](https://oer-informatik.de/anforderungsanalyse-dokumentieren)

- Haupttätigkeiten Validierung und Verwaltung: [Wie kann die Qualität der Anforderungen überprüft werden, wie priorisiere ich sie, und wie  verwalte ich sie im Projektverlauf?](https://oer-informatik.de/anforderungsanalyse-validieren-verwalten)

- [Übungsaufgaben zur Anforderungsanalyse](https://oer-informatik.de/anforderungsanalyse-uebungsaufgaben)

## Wozu dient die Anforderungsanalyse?

Projekte verfolgen bestimmte Ziele, die sie erreichen sollen - diese Ziele sind zum Teil offensichtlich, zum Teil versteckt.  Unabhängig vom gewählten Vorgehensmodell müssen diese Ziele (die Anforderungen an ein System) gesammelt und verstanden werden.

_Anforderungen_ definiert das _International Requirement Engineering Board_ (IREB) als: ^[[Glossar 2.0 des IREB](https://www.ireb.org/content/downloads/2-cpre-glossary-2-0/ireb_cpre_glossary_de_2.0.1.pdf)]

    A need perceived by a stakeholder.
    A capability or property that a system shall have.
    A documented representation of a need, capability or property

Diese Anforderungen müssen systematisch erhoben, dokumentiert, validiert und verwaltet werden. Diesen Prozess nennt man _Requirement Engineering_. Eine Übersetzung dafür könnte etwa Anforderungsanalyse sein. Was versteht man unter Anforderungsanalyse?^[Freie Übersetzung und Formulierung der Definition von _Requirements Engineering_ aus dem IREB Glossar]

* In der Anforderungsanalyse wird versucht zu verstehen und zu beschreiben, was die Anwender*Innen / Stakeholder (Interesseneigner*innen / Betroffene) vom zu erstellenden System wünschen oder brauchen. Es schafft eine gemeinsame Sprache und beugt Kommunikationsproblemen vor.

* Die Fähigkeiten und Aufgaben eines Software-Systems werden in seinem Nutzungskontext systematisch identifiziert und dokumentiert. Die erforderlichen Randbedingungen werden identifiziert und beschrieben.

* Die so erstellte Spezifikation wird kontinuierlich bearbeitet und gemanagt, um die Gefahr zu minimieren, dass das erstellte Softwaresystem nicht den Wünschen und Bedürfnissen entspricht. Risiken, durch frühzeitig entdeckte Fehler oder Spezifikationslücken werden so minimiert.

* Durch Ermittlung und Validierung der Anforderungen ist es möglich, den Projektaufwand realistischer abzuschätzen.

* Das Requirement Engineering ist ein unerlässlicher erster Schritt für Qualitätssicherung des Produkts und bildet die Grundlage für spätere Tests.

Der Aufwand, der für die Anforderungsanalyse eingeplant werden sollte, hängt von vielen Einflussfaktoren ab:

* Je größer das Risiko des Scheiterns eines Projekts und je größer der zu erwartende Schaden eines Scheiterns umso genauer sollte die Anforderungsanalyse durchgeführt werden.

* Je größer und diverser die Gruppe der _Stakeholder_, desto mehr Zeit sollte in das Verständnis der jeweiligen Anforderungen und das Lösen von Konflikten investiert werden.

## Welche Arten von Anforderungen gibt es?

Eine systematische Analyse verhindert, dass wesentliche Anforderungen vergessen werden. Eine eingängige Systematik, in welchen Bereichen Anforderungen ermittelt werden können, gibt beispielsweise die **FURPS+**-Einteilung wieder:

![FURPS - Abkürzung](images/FURPS.png)

FURPS+ steht hierbei als Akronym für:

* _**F**unctional_: Angemessenheit, Sicherheit, Interoperabilität, Konformität, Ordnungsmäßigkeit, Features, Fähigkeiten

* _**U**sability_ / Benutzerfreundlichkeit:  Human factors, Dokumentation, Attraktivität, Bedienbarkeit, Erlernbarkeit, Konformität, Verständlichkeit, Erlernbarkeit, Barrierefreiheit

* _**R**eliability_ / Zuverlässigkeit: Häufigkeit von Fehlern, Fehlererholung, Stabilität, Testbarkeit, Reife, Wiederherstellbarkeit, Nachweisbarkeit, Authentizität, Sicherheit (Safty & Security),  Vertraulichkeit, Integrität, Verhinderung von Gefahren für Mensch und Umwelt

* _**P**erformance_ / Effizienz: Antwortzeiten, Durchsatz, Verfügbarkeit, Kosten, Verbrauchsverhalten, Ressourcenbedarf

* _**S**upportability_ / Wartbarkeit: Analysierbarkeit, Modifizierbarkeit, Wiederverwertbarkeit, Internationalisierbarkeit, Skalierbarkeit, Prüfbarkeit, Austauschbarkeit, Installiertbarkeit, Übertragbarkeit

* "**+**" steht für weitere Anforderungen wie: _Implementation_ (Endliche Ressourcen, Sprachen und Tools, Hardware), _Interface_ (Schnittstellen für andere Systeme), _Operations_ (Systemmanagement), _Packaging_ (Strukturierung), _Legal_ (Lizenzen usw.)

Die Software-Qualitätsnorm ISO/IEC 25000 (vormals ISO/IEC 9126) gliedert die Qualitätsmerkmale in ähnlicher Weise wie die FURPS+-Systematik, dort spricht man von: Benutzbarkeit, Effizienz, Funktionalität, Übertragbarkeit, Wartbarkeit, Zuverlässigkeit, Sicherheit und Kompatibilität.

Da sich die funktionalen Anforderungen stark von den übrigen Anforderungen unterscheiden, werden diese beiden Gruppen gesondert behandelt: man unterteilt in funktionale Anforderungen (`F`) und nicht funktionalen Anforderungen (`URPS+`). Zu den nicht funktionalen Anforderungen zählen die Qualitätsanforderungen sowie die Randbedingungen. Auf alle Gruppen wird im Folgenden genauer eingegangen.

### Funktionale Anforderungen (_Functional requirements_)

Funktionale Anforderungen beantworten die Frage, was das System leisten soll. Hier werden angebotene Dienste ebenso beschrieben wie das Verhalten oder Fähigkeiten des Systems unter bestimmten Voraussetzungen.

Das IREB fasst funktionale Anforderungen so zusammen:

> _A requirement concerning a result or behavior that shall be provided by a function of a system._^[[IREB-Glossar zur Funktionalen Anforderung](https://www.ireb.org/content/downloads/2-cpre-glossary-2-0/ireb_cpre_glossary_de_2.0.1.pdf)]

Funktionale Anforderungen legen die Reaktion des Systems bei bestimmten Eingabewerten und Randbedingungen fest. Daher ist sowohl ihre Formulierung als auch die Zielüberprüfung relativ leicht objektivierbar. Sie können stark strukturiert oder natürlichsprachlich dokumentiert werden, beispielsweise in Form von Use-Cases (siehe unten) oder Szenarien mit Eingabewerten, Verarbeitungsschritten und Ausgabewerten.

### Qualitätsanforderungen (_Quality requirements_)

Qualitätsanforderungen (URPS+ oben) beantworten die Frage, wie das System die Funktionalen Anforderungen erfüllt.

Sie sind nicht offensichtlich, schwerer formulierbar und deren Zielerreichung oft sehr schwer objektivierbar. Da Qualitätsanforderungen bei der Abnahme und der Kundenzufriedenheit eine große Rolle spielen sind sie für den Projekterfolg sehr wichtig. Zu häufig werden sie lediglich mit Absichtserklärungen und weichen, nicht überprüfbaren  Formulierungen festgeschrieben ("soll leicht bedienbar sein", "intuitive Benutzeroberfläche"). Bei der Abnahme kann dies leicht zu Streitigkeiten und Unzufriedenheit führen. Daher sollten statt qualitativer Akzeptanzbedingungen ("Dass System muss performant sein.") in den Anforderungen überprüfbare und quantifizierbare Akzeptanzkriterien beschrieben werden ("Der Importvorgang einer Jahresrechnung darf nicht länger als 15 Sekunden dauern.").

Das IREB beschreibt die Qualitätsanforderungen so:

> _A requirement that pertains to a quality concern that is not covered by functional requirements._^[[IREB-Glossar zur Qualitätsanforderung](https://www.ireb.org/content/downloads/2-cpre-glossary-2-0/ireb_cpre_glossary_de_2.0.1.pdf)]

Es ist Aufgabe des Requirement Engineerings, geeignete Formulierungen und Metriken für QUalitätsanforderungen zu finden.

Eine detailliertere Liste von Qualitätsanforderungen findet sich in der ISO/IEC 25010:2011.

### Randbedingungen (_Constraints_)

Randbedingungen stellen keine Anforderungen dar, die das Projekt erreichen muss, sondern schränken die Realisierung des Projekts ein:

> _A requirement that limits the solution space beyond what is necessary for meeting the given functional requirements and quality requirements_^[[IREB-Glossar zur Randbedingungen](https://www.ireb.org/content/downloads/2-cpre-glossary-2-0/ireb_cpre_glossary_de_2.0.1.pdf)]

Diese Einschränkungen können unterschiedlichster Natur sein, u.a.:

- rechtliche Randbedingungen (Gesetze/Gesetzgeber)

- physikalisch/technische Randbedingungen (Normen, Naturgesetze...)

- kulturelle Randbedingungen (Sprache, Religion...)

- organisatorische Randbedingungen

- soziale / gesellschaftliche Randbedingungen

- politische Randbedingungen

- umweltbezogener Randbedingungen

- ökonomische / finanzielle Randbedingungen

- regionale Randbedingungen

Randbedingungen spiegeln sich häufig in Geschäftsanforderungen (z.B. wirtschaftliche Zielgrößen, Budget des Projekts) oder Domänenanforderungen (Umfeld des Projekts, vorhandene Technik,...) wider.

Anforderungen und Randbedingungen des gewünschten Systems sind oft zum Projektstart unbekannt und müssen durch die Anforderungsanalyse ermittelt werden.

## Haupttätigkeiten der Anforderungsanalyse (_requirements engineering_)

Anforderungen müssen über das Gesamtprojekt gemanagt werden. Hierzu gibt es unterschiedliche Vorgehensweisen und Gliederungen. Das _International Requirements Engineering Board_ unterscheidet vier Haupttätigkeiten, die als Phasen nacheinander oder parallel ablaufen können:

![Phasen des Requirement Engineering nach dem International Requirements Engineering Board](images/RequirementEngineering.png)

* Phase 1: Ermitteln der Anforderungen (_requirements elicitation_)

*	Phase 2: Dokumentieren der Anforderungen (_requirements documentation / specification_)

*	Phase 3: Prüfen und Bewerten der Anforderungen (_requirements validation_)

* Phase 4: Verwalten der Anforderungen, Anpassen und Ändern von Anforderungen im Prozess (_requirements management_)

Im gesamten Projektverlauf müssen die drei ineinander verzahnten Fragen beantwortet werden:

* Was ist das Ausgangsproblem?

* Welche Anforderungen ergeben sich daraus?

* Welche Lösungen können diese Anforderungen erfüllen?

(**Problem-Anforderung-Lösung - ein unausweichlich ineinandergreifendes Triple** - das 5. Prinzip des Requirement Engineerings nach IREB)

Die einzelnen Phasen und der Prozess werden projektspezifisch zugeschnitten, die Phasen können nacheinander oder parallel laufen und sind erst beendet, wenn das Projekt abgenommen ist und auch Wartungsphase und Auswertung beendet sind.

Ein Requirement Engineer wird den Prozess systematisch auf das jeweilige Projekt zuschneiden und in der Wahl der Prozesse und Praktiken die Erfordernisse des Projekts berücksichtigen. Prozesse und Praktiken werden stets reflektiert, bevor (und nachdem) sie eingesetzt werden. Das **Prinzip 9** des Requirement Engineering besagt: **Systematische und disziplinierte Arbeit - im Requirement Engineering unverzichtbar.**

## Wie geht es weiter?

Nachdem wir uns einen Überblick über den Gesamtprozess der Anforderungsanalyse verschafft haben, geht es im folgenden Blogpost zu...

[... der Anforderungsermittlung (_requirement elicitation_)](https://oer-informatik.de/anforderungsanalyse-ermittlung)

## Leitfragen

- Welche Phasen der Anforderungsanalyse gibt es?

## Weitere Literatur zu Requirement-Engineering

- Das deutschsprachige Handbuch des IREB (Martin Glinz, Hans van Loenhoud, Stefan Staal, Stan Bühne: Handbuch für das CPRE Foundation Level nach dem IREB-Standard) [findet sich hier als PDF](https://www.ireb.org/de/downloads/#cpre-foundation-level-handbook)

- Das Grundlagenbuch (auch zur Zertifizierung) für Anforderungsanalyse ist: Klaus Pohl, Chris Rupp: [Basiswissen Requirements Engineering](https://dpunkt.de/produkt/basiswissen-requirements-engineering/), dPunkt Verlag Heidelberg, ISBN 978-3-86490-814-9

- Weitere Unterlagen des International Requirement Engineering Boards zur Zertifizierung als "Certified Professional for Requirements Engineering Foundation Level": [https://www.ireb.org/de/cpre/foundation/](https://www.ireb.org/de/cpre/foundation/), insbesondere  [Lehrplan und Glossar](https://www.ireb.org/de/downloads/#cpre-foundation-level-handbook)


